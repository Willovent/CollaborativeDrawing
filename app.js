var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io').listen(http),
    browserify = require('browserify-middleware'),
    Datastore = require('nedb'),
    db = new Datastore({
        filename: 'data/db.json'
    });

//load & set db
db.loadDatabase();
db.persistence.setAutocompactionInterval(1000);
console.log('Hello');
http.listen(84);

app.use(express.static('public'));
app.get('/app.js', browserify('./public/script/main.js'));

var lastInterval;
var timeLeft = 60 * 10 * 1000 + (new Date().getTime());
    lastInterval = setInterval(function(){
        io.emit('timeLeft',Math.floor((timeLeft - (new Date().getTime()))/1000));
    },1000);
setInterval(function(){
    clearInterval(lastInterval);
    db.remove({}, { multi: true }, function (err, numRemoved) {});
    io.emit('clean');
    var timeLeft = 60 * 10 * 1000 + (new Date().getTime());
    lastInterval = setInterval(function(){
       io.emit('timeLeft',Math.floor((timeLeft - (new Date().getTime()))/1000));
    },1000);
},1000*60*10);

io.on('connection', function(client) {
    db.find({}).sort({ time: 1 }).exec(function(err, docs) {
            client.emit('init', docs);
    });

    client.on('newLine', function(line) {
        client.broadcast.emit('newLine', line);
        line.time = new Date().getTime();
        db.insert(line);
    });
    client.on('cheat',function(data){
    	client.broadcast.emit('cheat',data);
    });
});
