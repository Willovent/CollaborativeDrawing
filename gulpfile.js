var browserify = require('browserify'),
    gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    streamify = require('gulp-streamify'),
    source = require('vinyl-source-stream'),
    clean = require('gulp-clean'),
    deleteLines = require('gulp-delete-lines');

gulp.task('clean', function() {
    return gulp.src(['dist/*'], {
            read: false
        })
        .pipe(clean());
});

gulp.task('browserify', ['clean'], function() {
    return browserify('public/script/main.js')
        .bundle()
        .pipe(source('app.js'))
        .pipe(streamify(uglify()))
        .pipe(gulp.dest('dist/public'));
});
gulp.task('removeDebugScript' , ['copy','clean'] ,function(){
return gulp.src(['dist/app.js'])
    .pipe(deleteLines({
      'filters': [
      /browserify/i
      ]
    }))
    .pipe(gulp.dest('dist'));
    
});

gulp.task('copy',['clean'],function(){
	return gulp.src(['public/*.html','public/style/*','package.json','app.js'],{ base: './' })
	.pipe(gulp.dest('dist'));
})

gulp.task('default', ['copy','browserify','removeDebugScript'], function() {
   gulp.watch("public/*");
   gulp.watch("app.js");
});