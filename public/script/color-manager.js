module.exports = function() {
    this.R = {
        color: Math.round(Math.random() * 0xff),
        incr: true
    };
    this.G = {
        color: Math.round(Math.random() * 0xff),
        incr: true
    };
    this.B = {
        color: Math.round(Math.random() * 0xff),
        incr: true
    };
    this.getColor = function() {
        return "#" + Math.abs(this.R.color).toString(16) + Math.abs(this.G.color).toString(16) + Math.abs(this.B.color).toString(16)
    }
    this.incrColor = function() {
        this.incr(this.R);
        this.incr(this.G);
        this.incr(this.B);

    };
    this.incr = function(value) {
        if (value.incr) {
            value.color += Math.round(Math.random() * 10);
        } else {
            value.color -= Math.round(Math.random() * 10);
        }
        if (value.color > 0xf0)
            value.incr = false;
        if (value.color < 5)
            value.incr = true;
    }
}