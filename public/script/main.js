var canvas = document.querySelector('canvas');
canvas.height = 1000;
canvas.width = 1000;
var Color = require('./color-manager.js');
var Draw = require('./draw-manager.js'),
    draw = new Draw(canvas);
var ko = require('knockout');

window.viewModel = {
    min: ko.observable(0),
    sec: ko.observable(0),
    size: ko.observable(5),
    color: ko.observable('#000000')
}
var socketManager = require('./socket-manager.js'),
    socket = new socketManager(draw, viewModel);
var addDot = false;
var change = true;
var color = new Color();
var lastPoint = {};
var size = 0;
var changeScale = function() {
    size = innerHeight > innerWidth ? innerWidth : innerHeight;
    size -= 50;
    canvas.style.height = size + 'px';
    canvas.style.width = size + 'px';
    var range = document.querySelector('#range');
    if (innerHeight > innerWidth) {
        range.style['margin-top'] = size + 30 + 'px';
        range.style['transform'] = '';
        range.style['margin-left'] = '50%';
    } else {
        range.style['margin-left'] = -70 + ((innerWidth / 2)) + (size / 2) + 'px';
        range.style['transform'] = 'rotate(90deg)';
        range.style['margin-top'] = -50 + size / 2 + 'px';
    }
}
changeScale();


ko.applyBindings(viewModel);

var upEventHandler = function(event) {

    if (event.target == document.body || event.target == document.querySelector('canvas'))
        event.preventDefault();
    addDot = false;
};

var getPosition = function(event) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: (event.clientX !== undefined ? event.clientX - rect.left : event.changedTouches[0].clientX - rect.left) * (1000 / size),
        y: (event.clientY !== undefined ? event.clientY - rect.top : event.changedTouches[0].clientY - rect.top) * (1000 / size),
    };
};

var downEventHandler = function(event) {
    event.preventDefault();
    addDot = true;
    change = true;
    color = new Color();
    moveEventHandler(event);
};

var moveEventHandler = function(event) {
    event.preventDefault();
    if (addDot) {
        var newPosition = getPosition(event);

        if (!lastPoint.x || change){
            lastPoint.x = newPosition.x-1;
            lastPoint.y = newPosition.y-1;
        }
        color.incrColor();
        draw.addLine(lastPoint.x, lastPoint.y, newPosition.x, newPosition.y, viewModel.color() ? viewModel.color() : color.getColor(), viewModel.size());
        socket.sendNewLine({
            x1: lastPoint.x,
            y1: lastPoint.y,
            x2: newPosition.x,
            y2: newPosition.y,
            color: viewModel.color() ? viewModel.color() : color.getColor(),
            size: viewModel.size()
        });
        change = false;
        lastPoint = newPosition;
    }
};
var handleEvent = function() {
    canvas.addEventListener('mousedown', downEventHandler);
    canvas.addEventListener('touchstart', downEventHandler);

    window.addEventListener('mouseup', upEventHandler);
    canvas.addEventListener('touchend', upEventHandler);

    canvas.addEventListener('mousemove', moveEventHandler);
    canvas.addEventListener('touchmove', moveEventHandler);

    window.addEventListener('resize', changeScale);
};
handleEvent();
