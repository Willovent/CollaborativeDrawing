module.exports = function(canvas){
	var that = this;
	that.canvas = canvas;
	
	that.addLine = function(x1,y1,x2,y2,color,size){
		that.ctx.beginPath();
		that.ctx.lineWidth = size;
		that.ctx.strokeStyle = color;
		that.ctx.moveTo(x1, y1);
		that.ctx.lineTo(x2,y2); 
		that.ctx.closePath();
        that.ctx.stroke();
	};

	that.clean = function(){
		that.ctx.clearRect(0, 0, that.canvas.width, that.canvas.height);
	};

	that.ctx = canvas.getContext('2d');
	that.ctx.lineJoin="round";
};